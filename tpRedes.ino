
#include "ESP8266WiFi.h"
#include "ESP8266WebServer.h"
#include <OneWire.h>
#include <DallasTemperature.h>
#include <SPI.h> // comunicação SPI 
#include <SD.h> //protocolo SPI cartao SD

ESP8266WebServer server(80);
OneWire pino(D3);
DallasTemperature barramento(&pino);
DeviceAddress sensor;
const int chipSelect = 4; // Constante que indica em qual pino está conectado o Chip Select do módulo de comunicação
bool cartaoOk = true;
File dataFile;            // Objeto responsável por escrever/Ler do cartão SD
//WiFiClient cliente;


void setup() {
 
  Serial.begin(9600);
  barramento.begin();
  barramento.getAddress(sensor, 0); 
  WiFi.begin("LAB206", "capirotohuehue");
  
  while (WiFi.status() != WL_CONNECTED) { 
    delay(500);
    Serial.println("Waiting to connect…");
  }
 
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

   if (!SD.begin(chipSelect)) {
    Serial.println("Erro na leitura do arquivo não existe um cartão SD ou o módulo está conectado corretamente ?");
    cartaoOk = false;
    return;
  }
 
  server.on("/", indx);
 
  server.on("/temp", mandaTemp);    //Chama a funcao

  server.on("/log.csv", mandaLog);    

  server.on("/imagem.jpg", mandaIma1);

  server.on("/tsouto.jpg", mandaIma2);
  
  server.begin();                    //Start the server
  Serial.println("Server listening");
 
}
 
void loop() {

  server.handleClient();         //Chama a funcao certa callback, manipulando os pedidos

  if (cartaoOk){
      dataFile = SD.open("tempelog.csv", FILE_WRITE);
//      Serial.println("Cartão SD Inicializado");
  }
  String leitura = "";   
  float temperatura =  pegaTemp(); 
  leitura = String(millis()) + ";" + String(temperatura) + ";"; 

  if (dataFile) {   
    dataFile.println(leitura);  
    dataFile.close();          
  }

  delay(1500); 
 
}
 
float pegaTemp() {            
  String mensagem = "";
  barramento.requestTemperatures();
  return barramento.getTempC(sensor);
}

void mandaTemp() {            
  String mensagem = "";
  barramento.requestTemperatures(); 
  float temp = barramento.getTempC(sensor);
  mensagem = "A temperatura atual = ";
  mensagem += temp;
//  Serial.println(temperatura);
  server.send(200, "text/plain", mensagem);
}

void mandaLog(){
  String mensagem = "";
  dataFile = SD.open("TEMPELOG.CSV",FILE_READ);
  server.streamFile(dataFile,"text/csv");
  dataFile.close();

}

void indx(){
  String html = "<";
  dataFile = SD.open("INDE");
  if(dataFile){
    while(dataFile.available()){
      if(dataFile.read() == '\n'){
//        Serial.println(html);
          html += "\n";
        }else{
          html += dataFile.readString();
          }
      }
    }else{
      Serial.println("DEU RUIM NO SD");
      }
//    Serial.println(html);
    server.send(200, "text/html", html);
    dataFile.close();
  
 }

void mandaIma1(){
  dataFile = SD.open("ESP8266.JPG",FILE_READ);
  server.streamFile(dataFile,"image/jpg");
  dataFile.close();

}

void mandaIma2(){
  dataFile = SD.open("TSOUTO.JPG",FILE_READ);
  server.streamFile(dataFile,"image/jpg");
  dataFile.close();

}

